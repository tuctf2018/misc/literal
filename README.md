# Literal
Something's going over my head, and it's too fast for me to catch!
### Walkthrough
- curl webpage to find ASCII art of a 'fork bomb' in comments
- sift through to find unique characters that make up the flag, left to right top to bottom.